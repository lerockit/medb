import Vue from 'vue'
import VueRouter from 'vue-router'
import Notifications from 'vue-notification'

Vue.use(VueRouter)
Vue.use(Notifications)

import App from './views/App'
import ClientsIndex from './views/Clients/Index'
import ClientsCreate from './views/Clients/Create'
import ClientsShow from './views/Clients/Show'
import ClientsEdit from './views/Clients/Edit'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'clients.index',
            component: ClientsIndex,
        },
        {
            path: '/clients/create',
            name: 'clients.create',
            component: ClientsCreate,
        },
        {
            path: '/clients/:id',
            name: 'clients.show',
            component: ClientsShow,
        },
        {
            path: '/clients/:id/edit',
            name: 'clients.edit',
            component: ClientsEdit,
        }
    ],
});

const app = new Vue({
    el: '#app',
    components: { App },
    router,
});
