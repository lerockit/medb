import axios from 'axios';

const axiosBase = axios.create({
  baseURL: '/api',
});

export default {
  create(data) {
    return axiosBase.post('clients', data);
  },
  all() {
    return axiosBase.get('clients');
  },
  find(id) {
    return axiosBase.get(`clients/${id}`);
  },
  update(id, data) {
    return axiosBase.put(`clients/${id}`, data);
  },
  delete(id) {
    return axiosBase.delete(`clients/${id}`);
  },
};