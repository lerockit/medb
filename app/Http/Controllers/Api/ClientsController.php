<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Client;
use App\Mail\WelcomeEmail;
use Illuminate\Support\Facades\Mail;

class ClientsController extends Controller
{
    public function index()
    {
        return Client::all();
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'cpf' => 'required',
            'birthday' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'cep' => 'required',
            'address' => 'required',
            'city' => 'required',
            'state' => 'required',
        ]);
   
        Client::create($request->all());

        Mail::to($request->input('email'))->send(new WelcomeEmail($request->input('name')));

        return $request->all();
    }

    public function show(Client $client) 
    {
        return $client;
    }

    public function update(Request $request, Client $client)
    {
        $client->update($request->all());

        return response('', 200);
    }

    public function destroy(Client $client)
    {
        $client->delete();
        return response('', 204);
    }
}
