<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class WelcomeEmail extends Mailable
{
    use Queueable, SerializesModels;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function build()
    {
        return $this->from('lerockit.dev@gmail.com', 'MedB')
            ->subject('Bem-Vindo ao MedB')
            ->markdown('welcomeEmail')
            ->with([
                'name' => $this->name,
            ]);
    }
}
