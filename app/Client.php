<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = [
        'name',
        'cpf',
        'birthday',
        'email',
        'phone',
        'cep',
        'address',
        'city',
        'state',
    ];
}
